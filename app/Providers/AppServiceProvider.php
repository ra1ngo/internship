<?php

namespace App\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //от ошибки миграции на старых версиях mysql
        Schema::defaultStringLength(191);
        //директивы
        Blade::directive('option', function ($expression) {
            list($sort, $value) = explode(', ', $expression);

            $str = '<option value=' . $value
                . " <?php if ($sort==$value) echo 'selected';?>"
                . ' >';
            $value = str_replace(array("'", '"'), ' ', $value);
            $str = $str . $value . '</option>';

            return $str;
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
