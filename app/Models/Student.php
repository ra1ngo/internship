<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = [
        'name', 'surname', 'patronymic', 'birthday', 'gender'
    ];

    public function group()
    {
        return $this->belongsTo(Group::class);
    }

    public function subjects()
    {
        return $this->belongsToMany(Subject::class, 'ratings')
            ->withPivot('id', 'value');
    }

    public function scopeFilterOut($query, $data)
    {

        $query->orderBy($data['sort'], 'desc');

        if (isset($data['gender'])) {
            $query->where('gender', $data['gender']);
        }
        if (isset($data['group'])) {
            $query->where('group_id', $data['group']);
        }

        $arrlogic = array(
            null => null,
            'равно' => "=",
            'больше' => ">",
            'меньше' => "<"
        );
        $logic = $arrlogic[$data['logic']];
        if (isset($data['subject'])) {
            $query->join('ratings', 'students.id', '=', 'ratings.student_id')
                ->join('subjects', 'subjects.id', '=', 'ratings.subject_id')
                ->where('subjects.name', $data['subject'])
                ->where('ratings.value', $logic, $data['value'])
                ->select('students.*');

        }

        return $query;
    }
}
