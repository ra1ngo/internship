<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $fillable = ['title', 'description'];

    protected $hidden = [
        'id', 'parent_id', 'left', 'right', 'level'
    ];

    public function students()
    {
        return $this->hasMany(Student::class);
    }

    public function childs()
    {
        return $this->hasMany(Group::class, 'parent_id');
    }

    public function parent()
    {
        return $this->belongsTo(Group::class, 'parent_id');
    }
}
