<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Repositories\UserRepository;
use App\Models\Group;
use Tree;

class TreeComposer
{
    protected $users;
    
    public function __construct(Tree $tree)
    {
        // Зависимости автоматически извлекаются сервис-контейнером...
        $this->tree = $tree;
    }

    public function compose(View $view)
    {
        //Деревья
        $root = Group::where('parent_id', 0)->get();
        $groups = $this->tree->makeTree($root);
        /*Tree::shouldReceive('makeTree')
                        ->once()
                        ->with('root')
                        ->andReturn('groups');*/
        $view->with('groups', $groups);
    }
}