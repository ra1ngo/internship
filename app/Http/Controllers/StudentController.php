<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Student;

class StudentController extends Controller
{
    public function index(Request $request)
    {
        //$data = $request->all();  //не работает как надо
        //без этого придется ввыполнять кучу лишних проверок
        //blade не воспринимает неустановленные поля как null
        $data = [
            'sort' => $request->sort ?? 'name',
            'gender' => $request->gender ?? null,
            'group' => $request->group ?? null,
            'subject' => $request->subject ?? null,
            'logic' => $request->logic ?? null,
            'value' => $request->value ?? null,
        ];

        $students = Student::filterOut($data)->paginate(10);

        return view('index', [
            'students' => $students,
            'data' => $data,
        ]);
    }
}
