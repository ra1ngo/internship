<?php

namespace App\Helpers;

use App\Models\Group;

class Tree
{
    /////////////
    //Parent_ID//
    /////////////
    public function makeTree($nodes, $parent_id = 0)
    {
        foreach ($nodes as $node) {
            if ($node->parent_id == $parent_id) {
                $this->makeTree($node->childs, $node->id);
            }
        }

        return $nodes;
    }


    /////////////
    //NestedSet//
    /////////////
    public function makeTreeNS($root)
    {
        $left = $root->left - 1;
        $right = $root->right + 1;
        $nodes = Group::where([
            ['left', '>', $left],
            ['right', '<', $right],
            ['right', '>', 0],     //чтобы не показывать ноды с непроставленными индексами
        ])->get();

        return $nodes;
    }










    //--------------------------------------------------------------------
    //====================================================================
    //--------------------------------------------------------------------
    public function setLeftRight($nodes, $parent_id = 0, &$value = 0)
    {
        foreach ($nodes as $node) {
            if ($node->parent_id == $parent_id) {
                $node->left = $value++;
                $this->setLeftRight($node->childs, $node->id, $value);
                $node->right = $value++;
                $node->save();
            }
        }

        return $nodes;
    }


    public function insertNode($nodes, $parent_id, $new_id)
    {
        foreach ($nodes as $node) {
            if ($node->parent_id == $parent_id && $node->id != $new_id) {
                $node->parent_id = $new_id;
                $node->save();
            }
        }

        return true;
    }


    public function destroyNode($nodes, $id)
    {

        //удаляем ноду
        foreach ($nodes as $node) {
            if ($node->id == $id) {
                $parent_id = $node->parent_id;
                $node->delete();
            }
        }

        //меняем id ее прямых дочек
        foreach ($nodes as $node) {
            if ($node->parent_id == $id) {
                $node->parent_id = $parent_id;
                $node->save();
            }
        }

        return true;
    }


    public function destroyBranch($id, $del = false)
    {
        $nodes = $nodes = Group::all();
        $nodes = $nodes->keyBy('id');
        //$node = $nodes->get($request->id);
        //$node->delete();

        //удаляем ноду
        if (!$del) {
            foreach ($nodes as $node) {
                if ($node->id == $id) {
                    $node->delete();
                    $del = true;
                }
            }
        }


        //меняем id ее прямых дочек
        foreach ($nodes as $node) {
            if ($node->parent_id == $id) {
                $new_id = $node->id;
                $node->delete();

                $this->destroyBranch($new_id, $del);
            }
        }

        return true;
    }


    public function addNodeNS($parent_id, $name)
    {
        $parent = Group::where('id', $parent_id)->first();
        $right = $parent->right;

        //обновляем дочерние ноды
        $childs = Group::where('left', '>', $right)->get();
        foreach ($childs as $node) {
            $node->left += 2;
            $node->right += 2;
            $node->save();
        }

        //обновляем родительские ноды
        $parents = Group::where([
            ['right', '>=', $right],
            ['left', '<', $right],
        ])->get();
        foreach ($parents as $node) {
            $node->right += 2;
            $node->save();
        }

        //добавляем элемент
        $new_node = new Group;
        $new_node->parent_id = $parent_id;
        $new_node->left = $right;
        $new_node->right = $right + 1;
        $new_node->name = $name;
        $new_node->save();


        return true;
    }


    public function delNodeNS($id)
    {
        $deleted = Group::where('id', $id)->first();
        $right = $deleted->right;
        $left = $deleted->left;

        //удаляем узел (ветку)
        $nodes = Group::where([
            ['right', '<=', $right],
            ['left', '>=', $left],
        ])->get();
        foreach ($nodes as $node) {
            $node->delete();
        }

        //Обновление родительской ветки
        $parents = Group::where([
            ['right', '>', $right],
            ['left', '<', $left],
        ])->get();
        foreach ($parents as $node) {
            $node->right -= $right - $left + 1;
            $node->save();
        }

        //Обновление последующих узлов
        $childs = Group::where('left', '>', $right)->get();
        foreach ($childs as $node) {
            $node->left -= $right - $left + 1;
            $node->right -= $right - $left + 1;
            $node->save();
        }

        return true;
    }


    public function buildTree($parent_id, &$in, &$out = null)
    {
        $in = $in->keyBy('id');

        if (!isset($out)) {
            $out = collect();

            foreach ($in as $node) {
                if ($node->id == $parent_id) {
                    $parent_id = $node->id;
                    $out->push($node);
                    $in->forget($node->id);
                }
            }
        }
        $out = $out->keyBy('id');

        foreach ($in as $node) {
            if ($node->parent_id == $parent_id) {
                $new_parent_id = $node->id;


                $currentNode = $out->get($parent_id);

                if (empty($currentNode->children)) {
                    $currentNode->children = collect();
                }

                $currentNode->children->push($node);
                $currentNode->children = $currentNode->children->keyBy('id'); //для удобства
                $in->forget($node->id);
                $this->buildTree($new_parent_id, $in, $currentNode->children);
            }
        }

        return $out;
    }
}
