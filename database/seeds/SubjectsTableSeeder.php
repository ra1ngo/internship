<?php

use Illuminate\Database\Seeder;
use App\Models\Subject;
use App\Models\Student;

class SubjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $names = ['Матан', 'Русский язык', 'История'];
        factory(Subject::class, 3)->create()->each(function ($subject) use (&$names) {

            $subject->name = $names[0];
            array_splice($names, 0, 1);
            $subject->save();

            $students = Student::all();
            foreach ($students as $student) {
                $subject->students()->attach($student->id, ['value' => rand(1, 5)]);
            }

        });

    }
}
