<?php

use Illuminate\Database\Seeder;
use App\Models\Group;
use App\Models\Student;

class GroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //главные ноды дерева
        DB::table('groups')->insert([
            ['parent_id' => 0, 'title' => 'Россия'],
            ['parent_id' => 1, 'title' => 'МФИ'],
            ['parent_id' => 1, 'title' => 'УЛГУ'],
            ['parent_id' => 1, 'title' => 'Политех']
        ]);

        //создаем группы (вложенные ноды)
        factory(Group::class, 15)->create()->each(function ($group) {
            //создаем студентов и сразу связываем ч-з фабрику
            factory(Student::class, 10)->create([
                'group_id' => $group->id
            ]);
        });
    }
}
