<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('groups', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            //поля
            $table->string('title')->unique();
            $table->text('description')->nullable();

            $table->integer('parent_id')->unsigned()->index();
            $table->integer('left')->unsigned()->nullable();
            $table->integer('right')->unsigned()->nullable();
            $table->integer('level')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('groups');
    }
}
