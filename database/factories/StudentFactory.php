<?php

use Faker\Generator as Faker;
use App\Models\Student;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Student::class, function (Faker $faker, $attrib) {
    return [
        'name' => $faker->firstName,
        'surname' => $faker->lastName,
        'patronymic' => $faker->lastName,
        'birthday' => $faker->dateTime('2008-04-25 08:37:17'),
        'group_id' => $attrib['group_id'],
        'gender' => $faker->randomElement(['male', 'female'])
    ];
});