<?php

use Faker\Generator as Faker;
use App\Models\Group;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Group::class, function (Faker $faker) {
    return [
        'title' => $faker->company,
        'description' => $faker->text($maxNbChars = 200),
        'parent_id' => $faker->numberBetween(2, 4),
    ];
});