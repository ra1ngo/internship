@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">

                <div class="row">
                    <div class="container col-md-8">
                        <div class="card">
                            <div class="card-header">Поиск по фильтрам</div>
                            <div class="card-body">

                                <!-- Форма сортировки -->
                                <form action="{{ url('/') }}" method="GET" class="form-horizontal">
                                    <input type="hidden" name="group" value="{{ $data['group'] }}">

                                    <!-- Сортировать по -->
                                    <div class="form-group">
                                        <label for="sort">Сортировать по</label>
                                        <div class="col-sm-6">
                                            <select class="form-control" name="sort" id="FormControlSelect">
                                                @option($data['sort'], 'surname')
                                                @option($data['sort'], 'name')
                                                @option($data['sort'], 'birthday')
                                            </select>
                                        </div>
                                    </div>

                                    <!-- Оценка -->
                                    <div class="form-inline">
                                        <label for="sort">У кого оценка по предмету</label>
                                        <div class="col-sm-12">
                                            <select class="form-control" name="subject" id="FormControlSelect">
                                                @option($data['subject'], '')
                                                @option($data['subject'], 'Матан')
                                                @option($data['subject'], 'Русский язык')
                                                @option($data['subject'], 'История')
                                            </select>
                                            <select class="form-control" name="logic" id="FormControlSelect">
                                                @option($data['logic'], 'равно')
                                                @option($data['logic'], 'больше')
                                                @option($data['logic'], 'меньше')
                                            </select>
                                            <select class="form-control" name="value" id="FormControlSelect">
                                                @option($data['value'], '5')
                                                @option($data['value'], '4')
                                                @option($data['value'], '3')
                                            </select>
                                        </div>
                                    </div>

                                    <br>

                                    <!-- Пол -->
                                    <div class="form-group">
                                        <label for="sort">Пол</label>
                                        <div class="col-sm-6">
                                            <select class="form-control" name="gender" id="FormControlSelect">
                                                @option($data['gender'], '')
                                                @option($data['gender'], 'male')
                                                @option($data['gender'], 'female')
                                            </select>
                                        </div>
                                    </div>

                                    <br>
                                    <!-- Кнопка отправки результата -->
                                    <div class="form-group">
                                        <div class="col-sm-offset-3 col-sm-6">
                                            <button type="submit" class="btn btn-default">
                                                <i class="fa fa-plus"></i> Искать
                                            </button>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>

                        <br>

                        <div class="card">
                            <div class="card-header">Список студентов</div>

                            <div class="card-body">

                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Фамилия</th>
                                        <th>Имя</th>
                                        <th>Пол</th>
                                        <th>Матан</th>
                                        <th>Русский</th>
                                        <th>История</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach ($students as $student)
                                        <tr>
                                            <td>{{$student->surname}}</td>
                                            <td>{{$student->name}}</td>
                                            <td>{{$student->gender}}</td>
                                            <td>{{$student->subjects
                                        ->where('name', 'Матан')->first()
                                        ->pivot->value}}</td>
                                            <td>{{$student->subjects
                                        ->where('name', 'Русский язык')->first()
                                        ->pivot->value}}</td>
                                            <td>{{$student->subjects
                                        ->where('name', 'История')->first()
                                        ->pivot->value}}</td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>


                    <div class="container col-md-4">
                        <div class="card">
                            <div class="card-header">Дерево групп</div>
                            <div class="card-body">
                                @foreach ($groups as $root)
                                    {{$root->title}}
                                    <br>
                                    @foreach ($root->childs as $child)
                                        <br>
                                        <ul>
                                            {{$child->title}}
                                            @foreach ($child->childs as $group)



                                                <form action="{{ url('/') }}" method="GET" class="form-horizontal">
                                                    <input type="hidden" name="page"
                                                           value="{{ $students->currentPage() }}">
                                                    <input type="hidden" name="sort" value="{{ $data['sort'] }}">
                                                    <input type="hidden" name="gender" value="{{ $data['gender'] }}">
                                                    <input type="hidden" name="subject" value="{{ $data['subject'] }}">
                                                    <input type="hidden" name="logic" value="{{ $data['logic'] }}">
                                                    <input type="hidden" name="value" value="{{ $data['value'] }}">
                                                    <input type="hidden" name="group" value="{{ $group->id }}">

                                                    <!-- Кнопка отправки результата -->

                                                    <button type="submit" class="btn btn-link btn-sm">
                                                        <li>{{$group->title}}</li>
                                                    </button>

                                                </form>





                                            @endforeach
                                        </ul>

                                    @endforeach
                                @endforeach
                            </div>
                        </div>
                    </div>

                </div>

                <br>
                {{ $students->appends($data)->links() }}

            </div>
        </div>
    </div>
@endsection
