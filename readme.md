## Описание

Проект включает в себя решение двух стажерских заданий:
- сохранение состояния при фильтрации поиска
- деревья каталогов

Деревья используются в качестве дополнительного фильтра и полного взаимодействия с ними тут нет.
